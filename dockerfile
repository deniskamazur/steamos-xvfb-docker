FROM debian:stable
#FROM debian:stable

# update package list
RUN apt update -y
# install graphics-related packages
RUN apt install -y --no-install-recommends lightdm x11vnc xvfb i3 dmenu wmctrl

# prepare for steam installation
RUN sed -e 's/$/ non-free/' -i /etc/apt/sources.list\
    && dpkg --add-architecture i386\
    && apt -y update

# setup locales
RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen\
    && apt-get clean && apt-get update && apt-get install -y locales\
    && locale-gen en_US.UTF-8 &&\
    echo "export LANG=en_US.UTF8" > .profile

# install steam and dependencies for it
RUN apt install -y python curl libc6  python-apt xterm xz-utils zenity libgl1-mesa-dri:i386 libgl1-mesa-glx:i386 libc6:i386 wget \
    && cd /tmp\
    && wget https://steamcdn-a.akamaihd.net/client/installer/steam.deb\
    && dpkg -i steam.deb

# add new user
# TODO: username should be specified via an argument
RUN useradd -ms /bin/bash user\
    && usermod -aG sudo user
WORKDIR /home/user

# add i3 config file
COPY configs/i3config /home/user/.config/i3/config

# add bootstrap sript to container and make it runnable
COPY bootstrap.sh /home/user
RUN chmod +x /home/user/bootstrap.sh

USER user

CMD "/home/user/bootstrap.sh"
