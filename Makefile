DOCKER_ORGANIZATION := deniskamazur
DOCKER_IMAGE := steamos-xvfb

build-image:
	docker build -t $(DOCKER_ORGANIZATION)/$(DOCKER_IMAGE) .

run: build-image
	docker run -d -p 5900:5900 $(DOCKER_ORGANIZATION)/$(DOCKER_IMAGE)
